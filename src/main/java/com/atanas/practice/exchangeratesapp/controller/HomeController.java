package com.atanas.practice.exchangeratesapp.controller;

import com.atanas.practice.exchangeratesapp.entity.Quote;
import com.atanas.practice.exchangeratesapp.model.QuoteViewModel;
import com.atanas.practice.exchangeratesapp.repository.QuoteRepository;
import com.atanas.practice.exchangeratesapp.service.EntityOptimizer;
import com.atanas.practice.exchangeratesapp.service.JsonReader;
import com.atanas.practice.exchangeratesapp.service.ObjectMapperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

// TODO: Cleanup the code!
// TODO: Create more service to handle the repeating tasks.

/**
 * @author Atanas Minkov, naskospasko@gmail.com
 */
@RestController
public class HomeController {

    private static final String HISTORIC_DATE = "1999-01-04";

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    private final ObjectMapperService mapper;

    private final JsonReader reader;

    private final EntityOptimizer entityOptimizer;

    private final QuoteRepository repository;

    /**
     * Required dependencies are passed to the constructor. No field autowiring.
     *
     * @param mapper          "the object mapper service"
     * @param reader          "url reader service"
     * @param entityOptimizer "utility to optimize entities before persisting them in the DB"
     * @param repository      "Quote repository"
     */
    @Autowired
    public HomeController(
            ObjectMapperService mapper,
            JsonReader reader,
            EntityOptimizer entityOptimizer,
            QuoteRepository repository) {
        this.mapper = mapper;
        this.reader = reader;
        this.entityOptimizer = entityOptimizer;
        this.repository = repository;
    }

    /**
     * If you need to get the available currencies, you need to call this method by
     * requesting the route specified as value in the '@GetMapping' annotation. Add
     * it as suffix to the server's {address}:{port}.
     * example:'localhost:8080/currencies'
     *
     * @return List<String>
     * @throws IOException "handled by Spring framework"
     */
    @GetMapping("/currencies")
    public List<String> currenciesAction() throws IOException {
        List<String> result = new ArrayList<>();
        Quote viewModel;
        String jsonInString = reader.getResponseBody("http://api.fixer.io/latest");
        LOGGER.info("SUCCESS! JSON Fetched: {}", jsonInString);
        viewModel = mapper.readValue(jsonInString, Quote.class);
        result.add(viewModel.getBase());
        result.addAll(viewModel.getRates().keySet());
        return result;
    }

    /**
     * If you want to get the currency rates for specific date with a given base
     * currency, this method provides the data required.
     *
     * @param base "path variable {base}"
     * @param date "path variable {date}"
     * @return QuoteViewModel
     * @throws IOException "handled by Spring framework"
     */
    @GetMapping("/rates/historic/{base}/{date}")
    public QuoteViewModel historicBaseDateAction(@PathVariable(value = "base") String base,
                                                 @PathVariable(value = "date") String date) throws IOException {
        String jsonInString = reader.getResponseBody("http://api.fixer.io/" + date + "?base=" + base);
        LOGGER.info("SUCCESS! JSON Fetched: {}", jsonInString);
        Quote quote = mapper.readValue(jsonInString, Quote.class);
        QuoteViewModel viewModel = new QuoteViewModel(quote);
        if (!repository.existsByDate(quote.getDate())) {
            repository.save(entityOptimizer.optimize(quote));
        }
        return viewModel;
    }

    /**
     * This method is very similar to the one above. The sole difference is that it
     * uses fixed date, fetching the oldest available data from the rates API, in
     * this case - 1999-month-date.
     *
     * @param base "path variable {base}"
     * @return Quote
     * @throws IOException "handled by Spring framework"
     */
    @GetMapping("/rates/historic/{base}")
    public QuoteViewModel historicBaseAction(@PathVariable(value = "base") String base) throws IOException {
        String jsonInString = reader.getResponseBody("http://api.fixer.io/" + HISTORIC_DATE + "?base=" + base);
        LOGGER.info("SUCCESS! JSON Fetched: {}", jsonInString);
        Quote quote = mapper.readValue(jsonInString, Quote.class);
        QuoteViewModel viewModel = new QuoteViewModel(quote);
        if (!repository.existsByDate(quote.getDate())) {
            repository.save(entityOptimizer.optimize(quote));
        }
        return viewModel;
    }

    /**
     * In case you need the latest available data, this method is getting it form
     * the rates API.
     *
     * @param base "path variable {base}"
     * @return Quote
     * @throws IOException "handled by Spring framework"
     */
    @GetMapping("/rates/latest/{base}")
    public QuoteViewModel ratesLatestAction(@PathVariable(value = "base") String base) throws IOException {
        String jsonInString = reader.getResponseBody("http://api.fixer.io/latest" + "?base=" + base);
        LOGGER.info("SUCCESS! JSON Fetched: {}", jsonInString);
        Quote quote = mapper.readValue(jsonInString, Quote.class);
        QuoteViewModel viewModel = new QuoteViewModel(quote);
        if (!repository.existsByDate(quote.getDate())) {
            repository.save(entityOptimizer.optimize(quote));
        }
        return viewModel;
    }

    /**
     * This method iterates through the persisted Quote items
     * and then puts them together in a LinkedHashMap with String keys
     * indicating the date and BigDecimal values
     * compared against the base currency, that you type in the request URL.
     * The Map object is returned in the response body as JSON text
     * The results are grouped in per-year format (average, per year)
     *
     * @param currency "currency path variable"
     * @param date     "date path variable"
     * @return Map[year: value]
     */
    @GetMapping("/report/{currency}/{date}/per_year")
    public Map<String, BigDecimal> reportPerYearAction(@PathVariable String currency,
                                                       @PathVariable String date) {
        LocalDate requestDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate = LocalDate.now();
        if (requestDate.compareTo(endDate) > 0) return null;
        List<Quote> repositoryData = new ArrayList<>();
        repository.findAll().forEach(e -> {
            if (!(e.getDate().compareTo(requestDate) < 0)) {
                repositoryData.add(e);
            }
        });
        repositoryData.sort(Comparator.comparing(Quote::getDate));
        Map<String, BigDecimal> result = new LinkedHashMap<>();
        for (long year = requestDate.getYear(); year <= endDate.getYear(); year++) {
            BigDecimal sum = BigDecimal.ZERO;
            int entriesCount = 0;
            for (Quote quote : repositoryData) {
                boolean isBaseCurrency = quote.getBase().equals(currency);
                long quoteYear = quote.getDate().getYear();
                if (quoteYear != year) continue;
                sum = sum.add(isBaseCurrency ? BigDecimal.ONE : quote.getRates().get(currency));
                entriesCount++;
                result.put(String.valueOf(year), isBaseCurrency ? BigDecimal.ONE :
                        sum.divide(BigDecimal.valueOf(entriesCount), BigDecimal.ROUND_CEILING));
            }
        }
        return result;
    }

    /**
     * This method iterates through the persisted Quote items
     * and then puts them together in a LinkedHashMap with String keys
     * indicating the date and BigDecimal values
     * compared against the base currency, that you type in the request URL.
     * The Map object is returned in the response body as JSON text
     * The results are grouped in per-week format (average, per week)
     *
     * @param currency "currency path variable"
     * @param date     "date path variable"
     * @return Map[year: value]
     */
    @GetMapping("/report/{currency}/{date}/per_week")
    public Map<String, BigDecimal> reportPerWeekAction(@PathVariable String currency,
                                                       @PathVariable String date) {
        LocalDate requestDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate = LocalDate.now();
        if (requestDate.compareTo(endDate) > 0) return null;
        List<Quote> repositoryData = new ArrayList<>();
        repository.findAll().forEach(e -> {
            if (!(e.getDate().compareTo(requestDate) < 0)) {
                repositoryData.add(e);
            }
        });
        repositoryData.sort(Comparator.comparing(Quote::getDate));
        Map<String, BigDecimal> result = new LinkedHashMap<>();
        long endDateYear = endDate.getYear();
        for (long year = requestDate.getYear(); year <= endDateYear; year++) {
            BigDecimal sum = BigDecimal.ZERO;
            int weekOfYear = 0, entriesCount = 0;
            for (Quote quote : repositoryData) {
                long quoteYear = quote.getDate().getYear();
                if (quoteYear != year) continue;
                int newWeekOfYear = quote.getDate().getDayOfYear() / 7 + 1;
                boolean sameWeek = weekOfYear == newWeekOfYear;
                if (!sameWeek) weekOfYear = newWeekOfYear;
                BigDecimal addition = quote.getRates().get(currency);
                sum = sameWeek ? sum.add(addition) : addition;
                entriesCount = sameWeek ? entriesCount + 1 : 1;
                result.put(String.valueOf(year) + "-week: " + weekOfYear, sum.divide(BigDecimal.valueOf(entriesCount), BigDecimal.ROUND_CEILING));
            }
        }

        return result;
    }

    /**
     * This method iterates through the persisted Quote items
     * and then puts them together in a LinkedHashMap with String keys
     * indicating the date and BigDecimal values
     * compared against the base currency, that you type in the request URL.
     * The Map object is returned in the response body as JSON text
     * The results are grouped in per-day format
     *
     * @param currency "currency path variable"
     * @param date     "date path variable"
     * @return Map[year: value]
     */
    @GetMapping("/report/{currency}/{date}/per_day")
    public Map<String, BigDecimal> reportPerDayAction(@PathVariable String currency,
                                                      @PathVariable String date) {
        LocalDate requestDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate = LocalDate.now();
        if (requestDate.compareTo(endDate) > 0) return null;
        List<Quote> repositoryData = new ArrayList<>();
        repository.findAll().forEach(e -> {
            if (!(e.getDate().compareTo(requestDate) < 0)) {
                repositoryData.add(e);
            }
        });
        repositoryData.sort(Comparator.comparing(Quote::getDate));
        Map<String, BigDecimal> result = new LinkedHashMap<>();
        for (long year = requestDate.getYear(); year < endDate.getYear(); year++) {
            for (Quote quote : repositoryData) {
                result.put(quote.getDate().toString(), quote.getRates().get(currency));
            }
        }
        return result;
    }
}
