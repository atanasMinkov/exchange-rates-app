package com.atanas.practice.exchangeratesapp.repository;

import com.atanas.practice.exchangeratesapp.entity.Quote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface QuoteRepository extends CrudRepository<Quote, Long> {
    List<Quote> findByDate(LocalDate date);

    boolean existsByDate(LocalDate date);
}
