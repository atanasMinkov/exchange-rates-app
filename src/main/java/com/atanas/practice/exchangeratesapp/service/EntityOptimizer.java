package com.atanas.practice.exchangeratesapp.service;

import com.atanas.practice.exchangeratesapp.entity.Quote;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

/**
 * This utility optimizes the entity objects before storing them in the database.
 * This way, when querying them later, they'll be structured the way they are easiest to process.
 *
 * @author Atanas Minkov
 */
@Service
public class EntityOptimizer {
    public Quote optimize(Quote entity) {
        String optimalBase = "EUR";
        if (!entity.getBase().equals(optimalBase)) {
            Map<String, BigDecimal> rates = entity.getRates();
            BigDecimal newBaseValue = rates.get(optimalBase);
            rates.remove(optimalBase);
            rates.put(entity.getBase(), BigDecimal.ONE);
            entity.setBase(optimalBase);
            rates.entrySet().forEach(e -> e.setValue(e.getValue().
                    setScale(5, BigDecimal.ROUND_CEILING).
                    divide(newBaseValue, BigDecimal.ROUND_CEILING)));
        }
        return entity;
    }
}
