package com.atanas.practice.exchangeratesapp.model;

import com.atanas.practice.exchangeratesapp.entity.Quote;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is used to pass data to the client,
 * because entities can be modified just before persisting them in the database.
 */
public class QuoteViewModel {
    private String base;
    private LocalDate date;
    private Map<String, BigDecimal> rates;

    protected QuoteViewModel() {
    }

    public QuoteViewModel(String base, LocalDate date, Map<String, BigDecimal> rates) {
        this.base = base;
        this.date = date;
        this.rates = rates;
    }

    public QuoteViewModel(Quote quote) {
        this.base = quote.getBase();
        this.date = quote.getDate();
        this.rates = new HashMap<>(quote.getRates());
    }

    public String getBase() {
        return base;
    }

    public LocalDate getDate() {
        return date;
    }

    public Map<String, BigDecimal> getRates() {
        return rates;
    }

    public void setBase(String base) {
        this.base = base;
    }

}
