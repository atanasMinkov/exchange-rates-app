# Exchange Rates App - Work In Progress

Simple API that fetches currency exchange rates from fixer.io and caches them in a local DB

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The only thing you need is JDK8 and a DB provider

```
Java SE Development Kit 8 - Oracle or OpenJDK
MySQL 5.6 or higher.
```

### Building and running

This project uses Gradle as dependency and build lifecycle manager.

First clone this repo:

```
git clone https://github.com/fl3x1324/exchange-rates-app.git
```

Switch to the project root

```
cd exchange-rates-app
```

To initialize the DB, in Linux for example use this series of commands:
1. Start a mysql prompt as root:
```
sudo mysql -u root -p
```
2. Create schema:

```
create database db_exchange_rates;
```
3. Create user 'springuser' for this app with password 'ThePassword'. It's not cool to use the root user in production.
```
create user 'springuser'@'localhost' identified by 'ThePassword';
```
4. Give our new user 'springuser' all access rights to the newly created schema:
```
grant all on db_exchange_rates.* to 'springuser'@'localhost';
```
Setting up the database for this application is now complete. 

#### !!! IMPORTANT !!!
If you want to use custom username and password or your database is not on localhost 
or is not listening on the default port 3306, 
you'll need to modify the application.properties file to match your database settings

Use the project provided Gradle wrapper to build deployable jar file:

```
gradlew bootJar
```

Windows users can fire up the project by executing:

```
java -jar .\build\libs\exchange-rates-app-0.0.1-SNAPSHOT.jar
```
Linux/macOS users can fire up the project by executing:
```
java -jar ./build/libs/exchange-rates-app-0.0.1-SNAPSHOT.jar
```

You can now access the API by opening the following address in a web browser or Postman or your fronted app if you plan to use my backend app as your base
```
http://localhost:8080/currencies
```
This will give you a list with the currencies available from fixer.io
For more details about available actions and routes, please check the 
```
com.atanas.practice.exchangeratesapp.controller.HomeController.java
```
source file. I've written in detail about the app's functionality in this file.

## Deployment

To deploy this Spring Boot Application, all you need is the jar file. It's self sufficient as it contains all the dependencies it needs to run.

## Built With

* [Spring Boot](http://spring.io) - The web framework used
* [Gradle](https://gradle.org/) - Dependency Management

## Authors

* **Atanas Minkov** - *Initial work* - [fl3x1324](https://github.com/fl3x1324)
